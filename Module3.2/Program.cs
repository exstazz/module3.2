using System;
using System.Collections.Generic;
using System.Linq;

namespace Module3_2
{
    public class Program
    {
        static void Main(string[] args)
        {
            int b;
            int[] array = new int[5] { 1, 3, 2, 1, 8 };
            int[] array2 = new int[5] { 1, 3, 2, 1, 8 };
            Task4 task4 = new Task4();
            Task5 task5 = new Task5();
            Task6 task6 = new Task6();
            Task7 task7 = new Task7();
            Task8 task8 = new Task8();
            task4.TryParseNaturalNumber("1242", out b);
            task4.GetFibonacciSequence(-1);
            task5.ReverseNumber(312);
            task6.GenerateArray(-32845337);
            task6.UpdateElementToOppositeSign(array);
            task7.FindElementGreaterThenPrevious(array2);
            task8.FillArrayInSpiral(10);
        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            bool res;
            res = int.TryParse(input, out result);
            if (result % 1 == 0)
            {

            }
            else
            {
                Console.WriteLine("Попробуйте ещё раз");
            }
            Console.WriteLine(result);
            return res;
        }

        public int[] GetFibonacciSequence(int n)
        {
            int[] res = new int[0];
            if (n <= 0)
            {
                Console.WriteLine("Попробуйте снова");
            }
            else if (n == 1)
            {
                Console.WriteLine("Попробуйте снова");
            }
            else
            {

                res[0] = 0;
                res[1] = 1;
                for (int i = 2; i < n; ++i)
                {
                    res[i] = res[i - 1] + res[i - 2];
                }
                for (int i = 0; i < res.Length; i++)
                {
                    Console.WriteLine(res[i]);
                }
            }
            return res;
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            int res = 0;
            char[] replase;
            bool fortryparse;
            replase = (sourceNumber.ToString().Reverse().ToArray());
            fortryparse = int.TryParse(replase, out res);
            Console.WriteLine(res);
            return res;
        }
    }

    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            if (size <= 0)
            {
                Console.WriteLine("Попробуйте снова");
                int[] res = new int[1];
                return res;
            }
            else
            {
                int[] res = new int[size];
                return res;
            }
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            for (int i = 0; i < source.Length; i++)
            {
                source[i] = source[i] * -1;
                Console.WriteLine(source[i]);
            }
            return source;
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] res = new int[size];
            return res;
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> res = new List<int>();
            for (int i = 1; i < source.Length; i++)
            {
                if (source[i] > source[i - 1])
                {
                    res.Add(source[i]);
                }
            }
            for (int i = 0; i < res.Count; i++)
            {
                Console.WriteLine(res[i]);
            }
            return res;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            int N = size;
            int val = 1;
            int x = -1;
            int y = 0;
            int[,] array = new int[N, N];
            for (int i = 1; i < 2 * N; i++)
            {
                for (int j = 0; j < N - i / 2; j++)
                {
                    array[y += (i % 4 - 1) % 2, x -= (i % 4 - 2) % 2] = val++;
                }
            }

            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    Console.Write(array[i, j] + " ");
                }
                Console.WriteLine();
            }
            return array;
        }
    }
}